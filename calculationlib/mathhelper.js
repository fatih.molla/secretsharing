//<script type="text/javascript" src="MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
//<script type="text/javascript" src="jquery-1.10.2.js"></script>

var mathhelper = function() {
	$this = this;
	$this.out = {
	  write: function(container, tex) {
	    function createGuid(){
	      function S4() {
	  	  return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	  	}
	  	return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
	    }
	    var id = createGuid();
	    var div = container.append($('<div id=' + id + ' class="mathtex"></div>'))
	    //container.find('.mathtex').append($('<script type="math/tex">').text("\\displaystyle{"+tex+"}"));
			div.append($('<script type="math/tex">').text("\\displaystyle{"+tex+"}"));
			MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
	  },
	  clear: function(container) {
	    container.html("");
	  }
	};
	$this.read = {
	  matrix: function(text) {
	    var data = [];
		  text = text.replace( /\s\s+/g, ' ' );
		  var lines = text.split(/\r|\n/);
		  for (var i = 0; i < lines.length; i++) {
		    var lineData = [];
		    var cells = lines[i].split(' ');
		    for (var j = 0; j < cells.length; j++) {
			    if(cells[j] != "") {
			      lineData.push(parseInt(cells[j]));
			    }
		    }
		    if (lineData.length > 0)
		      data.push(lineData);
		  }
		  return data;
	  },
	  polynomial: function(text) {
	    parts = text.match(/([+-]|^).*?(?=([+-]|$))/g)
		  data = { };
		  for (var i = 0; i < parts.length; i++) {
		    var part = parts[i].split(' ').join('');
		    var spl = part.split('^');
		    if (spl.length == 1) {
          var pow = 0;
          if (spl[0].indexOf('x') >= 0)
            pow = 1;
          var coef = spl[0].replace('x', '').split(' ').join('');
          if (coef == '-') coef = -1;
          else if (coef == '' || coef == '+') coef = 1;
          else coef = parseInt(coef);
          data[pow] = (data[pow] | 0) + coef;
		    }
		    else if (spl.length == 2) {
		      var pow = parseInt(spl[1].replace(' ', ''));
          var coef = spl[0].replace('x', '').split(' ').join('');
          if (coef == '-') coef = -1;
          else if (coef == '' || coef == '+') coef = 1;
          else coef = parseInt(coef);
		      data[pow] = (data[pow] | 0) + coef;
		    }
		  }
      var keys = Object.keys(data);
      keys = keys.map(parseFloat).sort(function(a,b) { return a - b;}).reverse();
      //console.log(keys);

      var result = [];
      var max = keys[0];
      for (var i = max; i >= 0; i--) {
          result.push(data[i] | 0);
      }
      //console.log(result);
		  return result;
	  },
	};
}
