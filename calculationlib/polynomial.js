
var Polynomial = function(data, degree, symbol) {
	degree = new Degree(degree);
	symbol = symbol || 'x';

	if (!(Array.isArray(data)))
		data = [data];
	for (var i = 0; i < data.length; i++)
		data[i] = degree.Evaluate(data[i]);

	for (var i = 0; i < data.length; i++) {
		if (data[i] == 0) {
			data.splice(i, 1);
			i--;
		}
		else
			break;
	}

	if (data.length == 0)
		data.push(0);

	var text = "";
	var isAllCoeficientsZero = true;
	for(var i = 0; i < data.length; i++) {
		var coeficient = data[i];
		if (coeficient != 0)
			isAllCoeficientsZero = false;

		var power = data.length - i - 1;

		if (coeficient instanceof Polynomial) {
			if (i > 0)
				text += "+"
			text += "(" + coeficient.Text + ")";
			if(power > 0) {
				text += symbol;
				if(power != 1)
					text += "^" + "{" + power + "}";
			}
		}
		else if(coeficient != 0) {

			if (coeficient < 0)
				text += "-";
			else if (i > 0)
				text += "+";

			if(Math.abs(coeficient) != 1 || power == 0)
				text += Math.abs(coeficient);

			if(power > 0) {
				text += symbol;
				if(power != 1)
					text += "^" + "{" + power + "}";
			}
		}
	}
	if (isAllCoeficientsZero)
		text = "0";

	this.Symbol = symbol;
	this.Degree = degree;
	this.Data = data;
	this.Text = text;

	this.Calculate = function(x) {
		var result = 0;
		if (x instanceof Polynomial)
			result = new Polynomial([0], x.Degree, x.Symbol);
		for(var i = 0; i < data.length; i++) {
			var coeficient = data[i];
			var power = data.length - i - 1;
			if (x instanceof Polynomial) {
				result = result.Add(coeficient.Multiply(x.Pow(power, x.Symbol), coeficient.Symbol), result.Symbol)
			}
			else {
				result += coeficient * Math.pow(x, power);
				result = this.Degree.Evaluate(result);
			}
		}
		return result;
	}
	this.Clone = function() {
		var data = [];
		for (var i = 0; i < this.Data.length; i++)
			data.push(this.Data[i]);
		return new Polynomial(data);
	}
	this.Derivative = function(x){
		x = x || 1;
		var result = this.Clone();
		for (var i = 0; i < x; i++){
			var resultData = [];
			for (var j = 0; j < result.Data.length - 1; j++)
				resultData.push((result.Data.length - 1 - j) * result.Data[j]);
			result = new Polynomial(resultData, this.Degree.Value, this.Symbol);
		}
		return result;
	}
	this.Factorize = function() {

		return Factorization(this);

	}

	this.Multiply = function(operand, symbol){
		symbol = symbol || this.Symbol;
		var isPolyCoef = this.Data[0] instanceof Polynomial;
		if (!isPolyCoef) {
			if (!(operand instanceof Polynomial)){
				var result = [];
				for (var i = 0; i < this.Data.length; i++)
					result.push(this.Degree.Evaluate(this.Data[i] * operand));
				return new Polynomial(result, this.Degree, symbol);
			}
			else {
				var deg = (this.Data.length - 1) + (operand.Data.length - 1);
				var result = [];
				for (var i = 0; i < deg + 1; i++)
					result.push(0);
				for (var i = 0; i < this.Data.length; i++)
					for (var j = 0; j < operand.Data.length; j++) {
						var pow = (this.Data.length - 1 - i) + (operand.Data.length - 1 - j);
						var pos = deg - pow;
						coef = this.Data[i] * operand.Data[j];
						result[pos] = degree.Evaluate(result[pos] + coef);
					}
				return new Polynomial(result, this.Degree.Value, symbol);
			}
		}

		var isConstant = (operand.Symbol == this.Data[0].Symbol);

		if (isConstant){
			var result = [];
			for (var i = 0; i < this.Data.length; i++)
				result.push(this.Data[i].Multiply(operand));
			return new Polynomial(result, this.Degree, symbol);
		}
		else {
			var deg = (this.Data.length - 1) + (operand.Data.length - 1);
			var result = [];
			for (var i = 0; i < deg + 1; i++)
				result.push(new Polynomial([0], this.Data[0].Degree, this.Data[0].Symbol));
			for (var i = 0; i < this.Data.length; i++)
				for (var j = 0; j < operand.Data.length; j++) {
					var pow = (this.Data.length - 1 - i) + (operand.Data.length - 1 - j);
					var pos = deg - pow;
					coef = this.Data[i].Multiply(operand.Data[j]);
					result[pos] = result[pos].Add(coef);
				}
			return new Polynomial(result, this.Degree.Value, symbol);
		}
	}
	this.Add = function(operand, symbol) {
		var isPolyCoef = this.Data[0] instanceof Polynomial;
		if (!isPolyCoef) {
			if (!(operand instanceof Polynomial)){
				var result = [];
				for (var i = 0; i < this.Data.length; i++)
					result.push(this.Degree.Evaluate(this.Data[i] + (i == this.Data.length - 1 ? operand : 0))  );
				return new Polynomial(result, this.Degree, this.Symobol);
			}

			var deg = Math.max(this.Data.length - 1, operand.Data.length - 1);
			var result = [];
			for (var i = 0; i < deg + 1; i++)
				result.push(0);
			for (var i = deg; i >= 0; i--) {
				var ind1 = (this.Data.length - 1) - i;
				var ind2 = (operand.Data.length - 1) - i;
				var op1 = ind1 >= 0 ? this.Data[ind1] : 0;
				var op2 = ind2 >= 0 ? operand.Data[ind2] : 0;
				result[deg - i] = this.Degree.Evaluate(op1 + op2);
			}
			symbol = symbol || this.Symbol;
			return new Polynomial(result, this.Degree.Value, symbol);
		}

		var isConstant = (operand.Symbol == this.Data[0].Symbol);

		if (isConstant){
			var result = [];
			for (var i = 0; i < this.Data.length; i++)
				if (i == this.Data.length - 1)
					result.push(this.Data[i].Add(operand));
				else result.push(this.Data[i]);
			return new Polynomial(result, this.Degree, this.Symobol);
		}
		else {
			var deg = Math.max(this.Data.length - 1, operand.Data.length - 1);
			var result = [];
			for (var i = 0; i < deg + 1; i++)
				result.push(new Polynomial([0], this.Degree, this.Data[0].Symbol));
			for (var i = deg; i >= 0; i--) {
				var ind1 = (this.Data.length - 1) - i;
				var ind2 = (operand.Data.length - 1) - i;
				var op1 = ind1 >= 0 ? this.Data[ind1] : new Polynomial([0], this.Data[0].Degree, this.Data[0].Symbol);
				var op2 = ind2 >= 0 ? operand.Data[ind2] : new Polynomial([0], this.Data[0].Degree, this.Data[0].Symbol);
				result[deg - i] = op1.Add(op2);
			}
			symbol = symbol || this.Symbol;
			return new Polynomial(result, this.Degree.Value, symbol);
		}
	}
	this.Divide = function(operand) {
		operand = inverse(operand, this.Degree.Value);
		return this.Multiply(operand);
	}
	this.Pow = function(power, symbol) {
		symbol = symbol || this.Symbol;
		var result = new Polynomial([1], this.Degree, symbol);
		for (var i = 0; i < power; i++)
			result = result.Multiply(this, symbol);
		return result;
	}

	var inverse = function(value, mod){
		value = degree.Evaluate(value);
		for (var i = 1; i < mod; i++) {
			if (degree.Evaluate(value * i) == 1)
				return i;
		}
		return NaN;
	}

}


var Degree = function(degree) {

	if (degree instanceof Degree)
		this.Value = degree.Value;
	else this.Value = degree;
	this.Evaluate = function(value) {
		if (this.Value != null && !isNaN(this.Value)) {
			value = value % this.Value;
			if (value == 0)
				return 0;
			if (value < 0)
				value = this.Value + value;
		}
		return value;
	}
	this.Inverse = function(value) {
		value = this.Evaluate(value);
		for (var i = 1; i < this.Value; i++) {
			if (this.Evaluate(value * i) == 1)
				return i;
		}
		return NaN;
	}
}


var Interpolation = function(degree){

	this.Degree = degree;
	this.Polynomial = function(pairs){

		L = [];
		for (var k = 0; k < pairs.length; k++){
			var polynom = new Polynomial(1, this.Degree);
			for (var i = 0; i < pairs.length; i++) {
				if (k == i) continue;
				var xk = pairs[k][0];
				var xi = pairs[i][0];
				var divider = xk - xi;
				var p = new Polynomial([1, -1 * xi], this.Degree).Divide(divider);
				polynom = polynom.Multiply(p);
			}
			L.push(polynom);
		}

		var result = new Polynomial(0, this.Degree);
		for (var i = 0; i < L.length; i++)
			result = result.Add(L[i].Multiply(pairs[i][1]));
		return result;
	}

}


var Factorization = function(polynomial) {




}
