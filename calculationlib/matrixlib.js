var Matrix = function(a, errorMessage) {

	errorMessage = errorMessage || null;

	var data;
	var rowCount;
	var colCount;
	if(!Array.isArray(a)) {
		data = [[a]];
		rowCount = 1;
		colCount = 1;
	}
	else if (a.length > 0) {
		if(Array.isArray(a[0])){
			data = a;
			rowCount = a.length;
			colCount = a[0].length;
		}
		else {
			data = [a];
			rowCount = 1;
			colCount = a.length;
		}
	}
	else {
		data = [[]];
		rowCount = 0;
		colCount = 0;
	}

	this.Data = data;
	this.RowCount = rowCount;
	this.ColCount = colCount;
	this.Get = function(row,col) {
		return this.Data[row][col];
	}
	this.Set = function(row,col,val) {
		this.Data[row][col] = val;
	}
	this.GetRow = function(row) {
		return new Matrix(this.Data[row].slice(0));
	}
	this.GetCol = function(col) {
		var colData = [];
		for(var i = 0; i < this.RowCount; i++){
			colData.push(this.Data[i][col]);
		}
		return new Matrix(colData).Transpose();
	}
	this.GetRows = function(rows) {
		var result = [];
		for (var i = 0; i < rows.length; i++)
			result.push(this.GetRow(rows[i]));
		return new Matrix(result);
	}
	this.GetCols = function(cols) {
		var result = [];
		for(var i = 0; i < cols.length; i++)
			result.push(this.GetCol(cols[i]));
		return new Matrix(result).Transpose();
	}
	this.GetRowsBtw = function(start,end) {
		var result = [];
		for (var i = start; i <= end; i++)
			result.push(this.GetRow(i));
		return new Matrix(result);
	}
	this.GetColsBtw = function(start,end) {
		var result = [];
		for (var i = start; i <= end; i++)
			result.push(this.GetCol(i));
		return new Matrix(result).Transpose();
	}
	this.GetDiag = function() {
		var dim = Math.min(this.RowCount, this.ColCount);
		var data = [];
		for (var i = 0; i < dim; i++) {
			data.push(this.Get(i,i));
		}
		return new Matrix(data);
	}
	this.IsZero = function() {
		for (var i = 0; i < this.RowCount; i++){
			for (var j = 0; j < this.ColCount; j++){
				if (this.Get(i,j) != 0)
					return false;
			}
		}
		return true;
	}
	this.HasZero = function() {
		for (var i = 0; i < this.RowCount; i++){
			for (var j = 0; j < this.ColCount; j++){
				if (this.Get(i,j) == 0)
					return true;
			}
		}
		return false;
	}
	this.AddRight = function(matrix1) {
		if (rowCount == 0)
			return matrix1.Clone();
		var data = [];
		for (var i = 0; i < rowCount; i++) {
			var row = [];
			for (var j = 0; j < colCount; j++)
				row.push(this.Get(i,j));
			for (var j = 0; j < matrix1.ColCount; j++)
				row.push(matrix1.Get(i,j));
			data.push(row);
		}
		return new Matrix(data);
	}
	this.AddBottom = function(matrix1){
		var data = [];
		for (var i = 0; i < this.RowCount; i++)
			data.push(this.GetRow(i).Data[0]);
		for (var i = 0; i < matrix1.RowCount; i++)
			data.push(matrix1.GetRow(i).Data[0]);
		return new Matrix(data);
	}
	this.SwapRows = function(i, j) {
		var mat = this.Clone();
		var row = mat.GetRow(i).Data[0];
		for (var k = 0; k < mat.ColCount; k++)
			mat.Set(i, k, mat.Get(j, k));
		for (var k = 0; k < mat.ColCount; k++)
			mat.Set(j, k, row[k]);
		return mat;
	}
	this.ToLatex = function(writeDim) {
		if (errorMessage != null)
			return errorMessage;

		writeDim = writeDim || false;
		var result = "\\begin{bmatrix}";
		for (var i = 0; i < this.RowCount; i++)  {
			var line = "";
			for (var j = 0; j < this.ColCount; j++) {
				var value = this.Get(i, j);
				var isNumeric = !isNaN(parseFloat(value)) && isFinite(value);
				if (isNumeric) value = Math.round(value*100)/100;
					line += value + "&";
			}
			line = line.substr(0, line.length-1);
			result += line;
			if (i != this.RowCount-1)
				result += "\\\\";
		}
		//result = result.substr(0, result.length-2);
		result += "\\end{bmatrix}";
		if (writeDim)
			result += "_{" + this.RowCount + "\\times" + this.ColCount  + "}"
		return result;
	}
	this.ToString = function() {
		if (errorMessage != null)
			return errorMessage;

		var result = "[";
		for (var i = 0; i < this.RowCount; i++)  {
			var line = "[";
			for (var j = 0; j < this.ColCount; j++) {
				var value = this.Get(i, j);
				var isNumeric = !isNaN(parseFloat(value)) && isFinite(value);
				if (isNumeric) value = Math.round(value*100)/100;
					line += value + ",";
			}
			line = line.substr(0, line.length-1);
			result += line;
			result += "]";
			if (i != this.RowCount-1)
				result += ",";
		}
		result += "]";
		return result;
	}
	this.ToString2 = function() {
		if (errorMessage != null)
			return errorMessage;

		var result = "";
		for (var i = 0; i < this.RowCount; i++)  {
			var line = "";
			for (var j = 0; j < this.ColCount; j++) {
				var value = this.Get(i, j);
				var isNumeric = !isNaN(parseFloat(value)) && isFinite(value);
				if (isNumeric) value = Math.round(value*100)/100;
					line += value + " ";
			}
			line = line.substr(0, line.length-1);
			result += line;
			result += "\r\n";
			if (i != this.RowCount-1)
				result += "";
		}
		result += "";
		return result;
	}
	this.Transpose = function() {
		var data = [];
		for (var i = 0; i < this.ColCount; i++){
			var row = [];
			for (var j = 0; j < this.RowCount; j++)
				row.push(this.Get(j, i));
			data.push(row);
		}
		return new Matrix(data);
	}
	this.ToInt = function() {
		var data = [];
		for(var i = 0; i < this.RowCount; i++){
			var row = [];
			for(var j = 0; j < this.ColCount; j++){
				row.push(parseInt(this.Get(i, j)));
			}
			data.push(row);
		}
		return new Matrix(data);
	}
	this.Clone = function () {
		var data = [];
		for(var i = 0; i < this.RowCount; i++){
			var row = [];
			for(var j = 0; j < this.ColCount; j++){
				row.push(this.Get(i, j));
			}
			data.push(row);
		}
		return new Matrix(data);
	}
	this.IsSymmetric = function() {
		var rowCount = this.RowCount;
		var colCount = this.ColCount;
		var issymmetric = true;
		for (var j = 0; (j < rowCount) & issymmetric; j++) {
			for (var i = 0; (i < colCount) & issymmetric; i++) {
				issymmetric = (this.Data[i][j] == this.Data[j][i]);
			}
		}
		return issymmetric;
	}
	this.RemoveRow = function(row){
		var result = [];
		for (var i = 0; i < this.length; i++)
			if(i != row)
				result.push(this.GetRow(rows[i]));
		return new Matrix(result);
	}
	this.RemoveLastCol = function(){
		if (this.ColCount <= 1)
			return new Matrix([]);
		var result = this.GetCol(0);
		for (var i = 1; i < this.ColCount-1; i++)
				result = result.AddRight(this.GetCol(i));
		return result;
	}

}



var MatrixCalculator = function(degree) {

	this.Degree = degree;

	this.Multiply = function(op1, op2) {
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.MatrixMultiplication(op1, op2);
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar) { return matValue * scalar; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue) { return scalar * matValue; });
	}
	this.Add = function(op1, op2) {
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 + v2; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar) { return matValue + scalar; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue) { return scalar + matValue; });

	}
	this.Subtract = function(op1, op2) {
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 - v2; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar) { return matValue - scalar; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue) { return scalar - matValue; });
	}
	this.MultiplyP = function(op1, op2) {
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 * v2; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar) { return matValue * scalar; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue) { return scalar * matValue; });
	}
	this.DivideP = function(op1, op2) {
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return divide(v1, v2, degree); });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar) { return divide(matValue, scalar, degree); });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue) { return divide(scalar, matValue, degree); });
	}
	this.MatrixMultiplication = function(matrix1, matrix2) {
		if (matrix1.ColCount != matrix2.RowCount)
			return null;

		var data = [];
		for (var i = 0; i < matrix1.RowCount; i++) {
			var row = [];
			for (var j = 0; j < matrix2.ColCount; j++) {
				var value = 0;
				for(var k = 0; k < matrix1.ColCount; k++)
					value = value + matrix1.Get(i,k) * matrix2.Get(k, j);
				value = this.EvaluateDegree(value);
				row.push(value);
			}
			data.push(row);
		}
		return new Matrix(data);
	}

	this.IsScalerMultiplier = function(matrix1, matrix2) {
		if (matrix1 == null && matrix2 == null)
			return true;

		if (matrix1 == null || matrix2 == null)
			return false;

		if (matrix1.ColCount != matrix2.ColCount)
			return false;

		if (matrix1.RowCount != matrix2.RowCount)
			return false;

		var v = null;
		for (var i = 0; i < matrix1.RowCount; i++) {
			for (var j = 0; j < matrix1.ColCount; j++) {
				var v1 = matrix1.Get(i,j);
				var v2 = matrix2.Get(i,j);
				if (v1 == 0 && v2 == 0)
					continue;
				if (v1 == 0 || v2 == 0)
					return false;
				var r = divide(v1, v2, degree);
				if (v == null)
					v = r;
				if (v != r)
					return false;
			}
		}
		return true;
	}

	this.EQ = function(op1, op2, precision) {
		// Equal
		precision = precision || 1e-8;
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return Math.abs(v1 - v2) <= precision ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return Math.abs(matValue - scalar) <= precision ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return Math.abs(scalar - matValue) <= precision ? 1 : 0; });
	}
	this.NEQ = function(op1, op2, precision) {
		// Not Equal
		precision = precision != null ? precision : 1e-8;
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return Math.abs(v1 - v2) > precision ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return Math.abs(matValue - scalar) > precision ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return Math.abs(scalar - matValue) > precision ? 1 : 0; });
	}
	this.LT = function(op1, op2) {
		// Less Than
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 < v2 ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return matValue < scalar ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return scalar < matValue ? 1 : 0; });
	}
	this.LTE = function(op1, op2) {
		// Less Than Or Equal
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 <= v2 ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return matValue <= scalar ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return scalar <= matValue ? 1 : 0; });
	}
	this.GT = function(op1, op2) {
		// Greater Than
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 > v2 ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return matValue > scalar ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return scalar > matValue ? 1 : 0; });
	}
	this.GTE = function(op1, op2) {
		// Greater Than Or Equal
		if (op1 instanceof Matrix && op2 instanceof Matrix)
			return this.PointWiseOpearion(op1, op2, function(v1, v2) { return v1 >= v2 ? 1 : 0; });
		else if (op1 instanceof Matrix && !(op2 instanceof Matrix))
			return this.MatrixScalarOperation(op1, op2, function(matValue, scalar)  { return matValue >= scalar ? 1 : 0; });
		else if (!(op1 instanceof Matrix) && op2 instanceof Matrix)
			return this.ScalarMatrixOperation(op1, op2, function(scalar, matValue)  { return scalar >= matValue ? 1 : 0; });
	}

	var ExceptRowColumn = function(mat, rowIndex, colIndex) {

		var data = [];
		var mat1 = mat.Clone();
		for (var i = 0; i < mat1.RowCount; i++){
			if (i == rowIndex) continue;
			var row = mat1.Data[i];
			row.splice(colIndex, 1);
			data.push(row);
		}
		return new Matrix(data);
	}

	this.Sum = function(matrix) {
		return this.AggregateAll(matrix, 0, function(accumulate, value) { return accumulate + value; });
	}
	this.SumCols = function(matrix) {
		return this.AggregateCols(matrix, 0, function(accumulate, value) { return accumulate + value; });
	}
	this.SumRows = function(matrix) {
		return this.AggregateRows(matrix, 0, function(accumulate, value) { return accumulate + value; });
	}
	this.LU = function(mat) {
		return LU_Decomposition(mat,degree);
	}
	this.QR = function(mat) {
		return QR_Decomposition(mat,degree);
	}
	this.SVD = function(mat) {
		return SingularValue_Decomposition(mat, degree);
	}
	this.Eig = function (mat) {
		return Eigenvalue_Decomposition(mat, degree);
	}
	this.Chol = function(mat) {
		return Cholesky_Decomposition(mat,degree);
	}

	this.Determinant = function(mat) {
		if (mat.RowCount != mat.ColCount)
			return NaN;
		var path = [];
		return determinant(mat, path, this.Degree);
	}
	this.Solve = function(mat, result) {

		var matrix = mat.Clone().AddRight(result);
		for (var i = 0; i < matrix.RowCount; i++) {
			if (matrix.Get(i, i) == 0) {
				for (var j = i + 1; j < matrix.RowCount; j++)
					if (matrix.Get(j, i) != 0) {
						matrix = matrix.SwapRows(i, j);
						break;
					}
			}

			for (var j = i + 1; j < matrix.RowCount; j++) {
				if (matrix.Get(j, i) == 0)
					continue;
				var m = divide(matrix.Get(i, i), matrix.Get(j, i), this.Degree);
				for (var k = 0; k < matrix.ColCount; k++)
					matrix.Set(j, k, evaluate(m * matrix.Get(j, k) - matrix.Get(i, k), this.Degree));
			}
		}
		var result = [];
		for (var i = matrix.RowCount - 1; i >= 0; i--) {
			var coef = matrix.Get(i, i);
			var data = matrix.Get(i, matrix.ColCount - 1);
			var others = 0;
			for (var j = i + 1, k = 0; j < matrix.ColCount - 1; j++, k++)
				others += matrix.Get(i, j) * result[result.length - k - 1];
			result.push(divide(data - others, coef, this.Degree));
		}
		result = result.reverse();
		return new Matrix(result);
	}
	this.Eliminate = function(mat, result) {
		var matrix = mat;
		if (result != null)
			matric = mat.Clone().AddRight(result);
		for (var i = 0; i < matrix.RowCount; i++) {
			if (matrix.Get(i, i) == 0) {
				for (var j = i + 1; j < matrix.RowCount; j++)
					if (matrix.Get(j, i) != 0) {
						matrix = matrix.SwapRows(i, j);
						break;
					}
			}

			var v = matrix.Get(i,i);
			//var inv = inverse(v, this.Degree);
			for (var j = i; j < matrix.ColCount; j++)
				matrix.Set(i, j, divide(matrix.Get(i,j), v, this.Degree));

			for (var j = i + 1; j < matrix.RowCount; j++) {
				if (matrix.Get(j, i) == 0)
					continue;
				var m = divide(matrix.Get(i, i), matrix.Get(j, i), this.Degree);
				for (var k = 0; k < matrix.ColCount; k++)
					matrix.Set(j, k, evaluate(m * matrix.Get(j, k) - matrix.Get(i, k), this.Degree));
			}
		}
		return matrix;
	}
	this.Eliminate2 = function(mat) {
		var matrix = mat.Clone();
		//if (result != null)
		//	matric = mat.Clone().AddRight(result);
		for (var i = 0; i < matrix.RowCount; i++) {
			if (matrix.Get(i, i) == 0) {
				for (var j = i + 1; j < matrix.RowCount; j++)
					if (matrix.Get(j, i) != 0) {
						matrix = matrix.SwapRows(i, j);
						break;
					}
			}

			//var v = matrix.Get(i,i);
			//var inv = inverse(v, this.Degree);
			//for (var j = i; j < matrix.ColCount; j++)
			//	matrix.Set(i, j, divide(matrix.Get(i,j), v, this.Degree));

			for (var j = i + 1; j < matrix.RowCount; j++) {
				if (matrix.Get(j, i) == 0)
					continue;
				var m = divide(matrix.Get(i, i), matrix.Get(j, i), this.Degree);
				for (var k = 0; k < matrix.ColCount; k++)
					matrix.Set(j, k, evaluate(m * matrix.Get(j, k) - matrix.Get(i, k), this.Degree));
			}
		}
		return matrix;
	}

	this.PointWiseOpearion = function(matrix1, matrix2, op) {
		if(matrix1 == null || matrix2 == null)
			return new Matrix(0, "Matris boş olamaz.");
		if(matrix1.RowCount != matrix2.RowCount || matrix1.ColCount != matrix2.ColCount)
			return new Matrix(0, "Matrislerin boyutları eşit olmalı.");

		var data = [];
		for (var i = 0; i < matrix1.RowCount; i++) {
			var row = [];
			for (var j = 0; j < matrix1.ColCount; j++) {
				var v1 = matrix1.Get(i,j);
				var v2 = matrix2.Get(i,j);
				var value = op(v1, v2);
				value = this.EvaluateDegree(value);
				row.push(value);
			}
			data.push(row);
		}
		return new Matrix(data);
	}
	this.MatrixScalarOperation = function(mat, scalar, op) {
		var result = mat.Clone()
		for(var i = 0; i < result.RowCount; i++)
			for(var j = 0; j < result.RowCount; j++)
				result.Set(i,j,this.EvaluateDegree(op(result.Get(i,j), scalar)));
		return result;
	}
	this.ScalarMatrixOperation = function(scalar, mat, op) {
		var result = mat.Clone()
		for(var i = 0; i < result.RowCount; i++)
			for(var j = 0; j < result.RowCount; j++)
				result.Set(i,j,this.EvaluateDegree(op(scalar, result.Get(i,j))));
		return result;
	}

	this.AggregateCols = function(mat, seed, op) {
		var data = [];
		for (var i = 0; i < mat.RowCount; i++) {
			var accumulate = seed;
			for (var j = 0; j < mat.ColCount; j++) {
				var p1 = this.EvaluateDegree(mat.Get(i,j));
				accumulate = this.EvaluateDegree(op(accumulate, p1));
			}
			data.push([accumulate]);
		}
		return new Matrix(data);
	}
	this.AggregateRows = function(mat, seed, op) {
		var data = [];
		var accumulate = new MatrixGenerator().Constant(1, mat.ColCount, seed).GetRow(0).Data[0];
		for (var i = 0; i < mat.RowCount; i++) {
			for (var j = 0; j < mat.ColCount; j++) {
				var p1 = this.EvaluateDegree(mat.Get(i,j));
				accumulate[j] = this.EvaluateDegree(op(accumulate[j], p1));
			}
		}
		data.push(accumulate);
		return new Matrix(data);
	}
	this.AggregateAll = function(matrix, seed, op) {
		var accumulate = seed;
		for (var i = 0; i < matrix.RowCount; i++)
			for (var j = 0; j < matrix.ColCount; j++) {
				var p1 = this.EvaluateDegree(matrix.Get(i,j));
				accumulate = this.EvaluateDegree(op(accumulate, p1));
			}
		return accumulate;
	}
	this.EvaluateDegree = function(value) {
		if (this.Degree != null) {
			value = value % this.Degree;
			if (value == 0)
				return 0;
			if (value < 0)
				value = this.Degree + value;
		}
		return value;
	}
	this.Inverse = function(value) {
		return inverse(value, this.Degree);
	}

	var determinant = function(mat, path, degree) {

		var remainings = [];
		var l = path.length;
		for (var i = 0; i < mat.ColCount; i++)
			if (path.indexOf(i) < 0)
				remainings.push(i);

		if (mat.RowCount - 2 == path.length) {
			return evaluate(mat.Get(l, remainings[0]) * mat.Get(l + 1, remainings[1]) - mat.Get(l, remainings[1]) * mat.Get(l + 1, remainings[0]), degree);
		}
		else {
			var coef = 1;
			var sum = 0;
			for (var i = 0; i < remainings.length; i++){
				var pth = [];
				for (var k = 0; k < path.length; k++)
					pth.push(path[k]);
				pth.push(remainings[i]);

				var value = mat.Get(l, remainings[i]);
				var res = evaluate(coef * value * determinant(mat, pth, degree), degree);
				coef = -1 * coef;
				sum += res;
			}
			return evaluate(sum, degree);
		}
	}
	var evaluate = function(value, degree) {
		if (degree != null && !isNaN(degree)) {
			value = value % degree;
			if (value == 0)
				return 0;
			if (value < 0)
				value = degree + value;
		}
		return value;
	}
	var inverse = function(value, degree) {
		value = evaluate(value, degree);
		for (var i = 1; i < degree; i++) {
			if (evaluate(value * i, degree) == 1)
				return i;
		}
		return NaN;
	}
	var divide = function(x, y, degree) {
		if (degree != null && !isNaN(degree)) {
			var op = inverse(y, degree);
			return evaluate(x * op, degree);
		}
		else
		 	return x / y;
	}

}



var MatrixGenerator = function(degree) {

	this.Degree = degree;

	this.Pattern = function(rowCount, colCount, func) {
		var data = [];
		for(var i = 0; i < rowCount; i++){
			var row = [];
			for(var j = 0; j < colCount; j++){
				var val = func(i, j);
				val = this.EvaluateDegree(val);
				row.push(val);
			}
			data.push(row);
		}
		return new Matrix(data);
	}
	this.Constant = function(rowCount, colCount, value) {
		value = this.EvaluateDegree(value);
		var data = [];
		for (var i = 0; i < rowCount; i++) {
			var row = [];
			for (var j = 0; j < colCount; j++)
				row.push(value);
			data.push(row);
		}
		return new Matrix(data);
	}
	this.RandomInt = function(rowCount, colCount, start, end) {
		start = start || 0;
		end = end || 1;
		return this.Pattern(rowCount, colCount, function(i,j) { return parseInt(Math.random() * (end - start) + start); });
	}
	this.Random = function(rowCount, colCount) {
		return this.Pattern(rowCount, colCount, function(i,j) { return Math.random(); });
	}
	this.GetBaseMatrix = function (rowCount, colCount) {
		// not implemented
	}
	this.Vandermonde = function(colCount, vector) {
		var data = [];
		for (var i = 0; i < vector.length; i++) {
			var row = [];
			var val = vector[i];
			for (var j = 0; j < colCount; j++) {
				row.push(this.EvaluateDegree(Math.pow(val, j)));
			}
			data.push(row);
		}
		return new Matrix(data);
	}

	this.Identity = function(rowCount, colCount) {
		var data = [];
		for (var i = 0; i < rowCount; i++){
			var row = [];
			for (var j = 0; j < colCount; j++){
				if (i == j)
					row.push(1);
				else row.push(0);
			}
			data.push(row);
		}
		return new Matrix(data);
	}
	this.EvaluateDegree = function(value) {
		if (this.Degree != null) {
			value = value % this.Degree;
			if (value < 0)
				value = this.Degree + value;
		}
		return value;
	}

}




var LU = function(mat, lu, pivot, pivsign) {
	var rowCount = lu.RowCount;
	var colCount = lu.ColCount;

	var l = [];
	for(var i = 0; i < rowCount; i++) {
		l.push([]);
		for(var j = 0; j < colCount; j++) {
			if (i > j) l[i][j] = lu.Get(i,j);
			else if (i == j) l[i][j] = 1;
			else l[i][j] = 0;
		}
	}

	var u = [];
	for (var i = 0; i < colCount; i++) {
		u.push([]);
		for (var j = 0; j < colCount; j++) {
			if (i <= j) u[i][j] = lu.Get(i,j);
			else u[i][j] = 0;
		}
	}

	var isNonsingular = true;
	for (var j = 0; j < colCount; j++)
		if (lu.Get(j,j) == 0) {
			isNonsingular = false;
			break;
		}

	var determinant = pivsign;
	for (var j = 0; j < colCount; j++)
		determinant *= lu.Get(j,j);

	this.L = new Matrix(l);
	this.U = new Matrix(u);
	this.Pivot = new Matrix(pivot);
	this.IsNonsingular = isNonsingular;
	this.Determinant = determinant;
	this.Solve = function(result, degree) {
		// result is column matrix
		var y = result.Transpose().Data[0];
		var x = [];
		for (var i = 0; i < y.length; i++)
			x.push(y[pivot[i]]);

		for (var k = 0; k < mat.ColCount; k++)
			for (var i = k + 1; i < mat.ColCount; i++)
				x[i] -= x[k] * lu.Get(i, k);

		for (var k = mat.ColCount - 1; k >= 0; k--) {
			x[k] = divide(x[k], lu.Get(k,k));
			for (var i = 0; i < k; i++)
				x[i] -= x[k] * lu.Get(i, k);
		}
		return new Matrix(x).Transpose();
	}

	var evaluate = function(value, degree) {
		if (degree != null && !isNaN(degree)) {
			value = value % degree;
			if (value == 0)
				return 0;
			if (value < 0)
				value = degree + value;
		}
		return value;
	}
	var inverse = function(value, degree){
		if (degree != null && !isNaN(degree)) {
			value = evaluate(value, degree);
			for (var i = 1; i < degree; i++) {
				if (evaluate(value * i) == 1)
					return i;
			}
			return NaN;
		}
		return value;
	}
	var divide = function(x, y, degree) {
		if (degree != null && !isNaN(degree)) {
			return evaluate(x * inverse(y, degree), degree);
		}
		else {
			return x / y;
		}
	}
}
var LU_Decomposition = function(mat, degree) {
	var matGen = new MatrixGenerator(degree);

	var lu = mat.Clone();
	var rowCount = lu.RowCount;
	var colCount = lu.ColCount;
	var pivsign = 1;
	var pivot = matGen.Pattern(1, rowCount, function (i,j) { return j; }).GetRow(0);
	var LUrowi;
	var LUcolj = matGen.Constant(1, rowCount).GetRow(0);
	// outer loop
	for (var j = 0; j < colCount; j++) {
		// Make a copy of the j-th column to localize referances.
		for (var i = 0; i < rowCount; i++)
			LUcolj[i] = lu.Get(i,j);

		// Apply previous transformations.
		for (var i = 0; i < rowCount; i++){
			LUrowi = lu.GetRow(i);

			// Most of the time is spent in the following dot product.
			var kmax = Math.min(i, j);
			var s = 0.0;
			for (var k = 0; k < kmax; k++)
				s += LUrowi[k] * LUcolj[k];
			LUrowi[j] = LUcolj[i] -= s;
		}
		var p = j;
		for (var i = j + 1; i < rowCount; i++)
			if (Math.abs(LUcolj[i]) > Math.abs(LUcolj[p]))
				p = i;

		if (p != j) {
			for (var k = 0; k < colCount; k++) {
				var t = lu.Get(p,k);
				lu.Set(p,k,lu.Get(j,k));
				lu.Set(j,k,t);
			}
			var kk = pivot[p];
			pivot[p] = pivot[j];
			pivot[j] = kk;
			pivsign = -pivsign;
		}
		// Compute multipliers.
		if (j < rowCount & lu.Get(j,j) != 0.0)
			for (var i = j + 1; i < rowCount; i++)
				lu.Set(i,j, lu.Get(i,j)/lu.Get(j,j));
	}

	return new LU(mat, lu, pivot, pivsign, rowCount, colCount);
}



var QR = function(qr, rdiag, degree) {
	var rowCount = qr.RowCount;
	var colCount = qr.ColCount;
	var gen = new MatrixGenerator(degree);

	var q = gen.Constant(rowCount, colCount, 0).Data;
	for(var k = colCount - 1; k >= 0; k--) {
		for(var i = 0; i < rowCount; i++)
			q[i][k] = 0.0;
		q[k][k] = 1.0;

		for(var j = k; j < colCount; j++) {
			if(qr.Get(k,k) != 0) {
				var s = 0.0;
				for(var i = k; i < rowCount; i++)
					s += qr.Get(i,k) * q[i][j];
				s = -s / qr.Get(k,k);
				for(var i = k; i < rowCount; i++)
					q[i][j] += s * qr.Get(i,k);
			}
		}
	}

	var r = [];
	for(var i = 0; i < colCount; i++) {
		r.push([]);
		for(var j = 0; j < colCount; j++) {
			if (i < j)
				r[i][j] = qr.Get(i,j);
			else if (i == j)
				r[i][j] = rdiag[i];
			else
				r[i][j] = 0.0;
		}
	}

	var h = [];
	for(var i = 0; i < rowCount; i++) {
		h.push([]);
		for(var j = 0; j < colCount; j++) {
			if(i >= j) h[i][j] = qr.Get(i,j);
			else h[i][j] = 0.0;
		}
	}

	isFullRank = true;
	for(var j = 0; j < colCount; j++)
		if(rdiag[j] == 0) {
			isFullRank = false;
			break;
		}

	this.Q = new Matrix(q);
	this.R = new Matrix(r);
	this.H = new Matrix(h);
	this.IsFullRank = isFullRank;
}
var QR_Decomposition = function(mat, degree) {
	var gen = new MatrixGenerator(degree);

	var rowCount = mat.RowCount;
	var colCount = mat.ColCount;
	var qr = mat.Clone();
	var rdiag = gen.Constant(1, colCount, 0);

	var hypot = function(a, b) {
		var r;
		if (Math.abs(a) > Math.abs(b)){
			r = b / a;
			r = Math.abs(a) * Math.sqrt(1 + r*r);
		}
		else if (b != 0) {
			r = a / b;
			r = Math.abs(b) * Math.sqrt(1 + r*r);
		}
		else {
			r = 0;
		}
		return r;
	}

		// Main loop.
	for (var k = 0; k < colCount; k++) {
		// Compute 2-norm of k-th column without under/overflow.
		var nrm = 0;
		for (var i = k; i < rowCount; i++)
			nrm = hypot(nrm, qr.Get(i,k));

		if (nrm != 0.0) {
			// Form k-th Householder vector.
			if (qr.Get(k,k) < 0)
				nrm = -nrm;

			for (var i = k; i < rowCount; i++)
				qr.Set(i,k, qr.Get(i,k)/nrm);
			qr.Set(k,k, qr.Get(k,k) + 1.0);

			// Apply transformation to remaining columns.
			for (var j = k + 1; j < colCount; j++) {
				var s = 0.0;
				for (var i = k; i < rowCount; i++)
					s += qr.Get(i,k) * qr.Get(i,j);
				s = -s / qr.Get(k,k);
				for (var i = k; i < rowCount; i++)
					qr.Set(i,j, qr.Get(i,j) + s * qr.Get(i,k));
			}
		}
		rdiag[k] = -nrm;
	}

	return new QR(qr, rdiag, degree);
}

var Cholesky = function(l, isspd, degree) {
	this.L = l;
	this.IsSPD = isspd;
}
var Cholesky_Decomposition = function(mat, degree) {
	var matGen = new MatrixGenerator(degree);
	var a = mat.Clone();
	var rowCount = a.RowCount;
	var colCount = a.ColCount;
	var isspd = (rowCount == colCount);
	var l = matGen.Constant(rowCount, colCount, 0).Data;
	// Main loop.
	for (var j = 0; j < rowCount; j++) {
		var Lrowj = l[j];
		var d = 0.0;
		for (var k = 0; k < j; k++) {
			var Lrowk = l[k];
			var s = 0.0;
			for (var i = 0; i < k; i++)
				s += Lrowk[i] * Lrowj[i];

			Lrowj[k] = s = (a.Get(j,k) - s) / l[k][k];
			d = d + s * s;
			isspd = isspd & (a.Get(k,j) == a.Get(j,k));
		}
		d = a.Get(j,j) - d;
		isspd = isspd & (d > 0.0);
		l[j][j] = Math.sqrt(Math.max(d, 0.0));
		for (var k = j + 1; k < rowCount; k++)
			l[j][k] = 0.0;
	}
	return new Cholesky(new Matrix(l), isspd);
}

var Eig = function(rowCount, vv, dd, ee, degree) {
	var matGen = new MatrixGenerator(degree);
	var d = matGen.Constant(rowCount, rowCount).Data;
	for (var i = 0; i < rowCount; i++) {
		for (var j = 0; j < rowCount; j++)
			d[i][j] = 0.0;
		d[i][i] = dd[i];
		if (ee[i] > 0) {
			d[i][i + 1] = ee[i];
		}
		else if (ee[i] < 0) {
			d[i][i - 1] = ee[i];
		}
	}

	this.D = new Matrix(d);
	this.V = new Matrix(vv);
	this.RealEigenvalues = new Matrix(dd);
	this.ImagEigenvalues = new Matrix(ee);
}
var Eigenvalue_Decomposition = function(mat, degree) {

	var cdiv = function(cdiv1,xr,xi,yr,yi) {
		var r, d;
		if (Math.abs(yr) > Math.abs(yi)) {
			r = yi / yr;
			d = yr + r * yi;
			cdiv1.r = (xr + r * xi) / d;
			cdiv1.i = (xi - r * xr) / d;
		}
		else {
			r = yr / yi;
			d = yi + r * yr;
			cdiv1.r = (r * xr + xi) / d;
			cdiv1.i = (r * xi - xr) / d;
		}
		return cdiv1;
	}
	var tred2 = function(rowCount,vars) {
		var vv = vars.vv;
		var dd = vars.dd;
		var ee = vars.ee;

		for (var j = 0; j < rowCount; j++)
			dd[j] = vv[rowCount - 1][j];

		// Householder reduction to tridiagonal form.
		for (var i = rowCount - 1; i > 0; i--) {
			// Scale to avoid under/overflow.
			var scale = 0.0;
			var h = 0.0;
			for (var k = 0; k < i; k++)
				scale = scale + Math.abs(dd[k]);

			if (scale == 0.0) {
				ee[i] = dd[i - 1];
				for (var j = 0; j < i; j++) {
					dd[j] = vv[i - 1][j];
					vv[i][j] = 0.0;
					vv[j][i] = 0.0;
				}
			}
			else {
				// Generate Householder vector.
				for (var k = 0; k < i; k++) {
					dd[k] /= scale;
					h += dd[k] * dd[k];
				}
				var f = d[i - 1];
				var g = Math.sqrt(h);
				if (f > 0)
					g = -g;
				ee[i] = scale * g;
				h = h - f * g;
				dd[i - 1] = f - g;
				for (var j = 0; j < i; j++)
					ee[j] = 0.0;

				// Apply similarity transformation to remaining columns.
				for (var j = 0; j < i; j++) {
					f = dd[j];
					vv[j][i] = f;
					g = ee[j] + vv[j][j] * f;
					for (var k = j + 1; k <= i - 1; k++) {
						g += vv[k][j] * dd[k];
						ee[k] += vv[k][j] * f;
					}
					ee[j] = g;
				}
				f = 0.0;
				for (var j = 0; j < i; j++) {
					ee[j] /= h;
					f += ee[j] * dd[j];
				}

				var hh = f / (h + h);
				for (var j = 0; j < i; j++)
					ee[j] -= hh * dd[j];

				for (var j = 0; j < i; j++) {
					f = dd[j];
					g = ee[j];
					for (var k = j; k <= i - 1; k++)
						vv[k][j] -= (f * ee[k] + g * dd[k]);
					dd[j] = vv[i - 1][j];
					vv[i][j] = 0.0;
				}
			}
			dd[i] = h;
		}

		// Accumulate transformations.
		for (var i = 0; i < rowCount - 1; i++) {
			vv[rowCount - 1][i] = vv[i][i];
			vv[i][i] = 1.0;
			var h = dd[i + 1];
			if (h != 0.0) {
				for (var k = 0; k <= i; k++)
					dd[k] = vv[k][i + 1] / h;

				for (var j = 0; j <= i; j++) {
					var g = 0.0;
					for (var k = 0; k <= i; k++)
						g += vv[k][i + 1] * vv[k][j];
					for (var k = 0; k <= i; k++)
						vv[k][j] -= g * dd[k];
				}
			}
			for (var k = 0; k <= i; k++)
				vv[k][i + 1] = 0.0;
		}

		for (var j = 0; j < rowCount; j++) {
			dd[j] = vv[rowCount - 1][j];
			vv[rowCount - 1][j] = 0.0;
		}
		vv[rowCount - 1][rowCount - 1] = 1.0;
		ee[0] = 0.0;

		vars.vv = vv;
		vars.dd = vv;
		vars.ee = vv;
		return vars;
	}
	var tql2 = function(rowCount,vars) {
		var vv = vars.vv;
		var ee = vars.ee;
		var dd = vars.dd;

		for (var i = 1; i < rowCount; i++)
			ee[i - 1] = ee[i];
		ee[rowCount - 1] = 0.0;

		var f = 0.0;
		var tst1 = 0.0;
		var eps = Math.pow(2.0, -52.0);
		for (var l = 0; l < rowCount; l++) {
			// Find small subdiagonal element
			tst1 = Math.Max(tst1, Math.abs(dd[l]) + Math.abs(ee[l]));
			var m = l;
			while (m < rowCount) {
				if (Math.abs(ee[m]) <= eps * tst1)
					break;
				m++;
			}

			// If m == l, d[l] is an eigenvalue, otherwise, iterate.
			if (m > l) {
				var iter = 0;
				do {
					iter = iter + 1;  // (Could check iteration count here.)

					// Compute implicit shift
					var g = dd[l];
					var p = (dd[l + 1] - g) / (2.0 * ee[l]);
					var r = Maths.hypot(p, 1.0);
					if (p < 0)
						r = -r;

					dd[l] = ee[l] / (p + r);
					dd[l + 1] = ee[l] * (p + r);
					var dl1 = dd[l + 1];
					var h = g - dd[l];
					for (var i = l + 2; i < rowCount; i++)
						dd[i] -= h;

					f = f + h;

					// Implicit QL transformation.
					p = dd[m];
					var c = 1.0;
					var c2 = c;
					var c3 = c;
					var el1 = ee[l + 1];
					var s = 0.0;
					var s2 = 0.0;
					for (var i = m - 1; i >= l; i--) {
						c3 = c2;
						c2 = c;
						s2 = s;
						g = c * ee[i];
						h = c * p;
						r = Maths.hypot(p, ee[i]);
						ee[i + 1] = s * r;
						s = ee[i] / r;
						c = p / r;
						p = c * dd[i] - s * g;
						dd[i + 1] = h + s * (c * g + s * dd[i]);

						// Accumulate transformation.
						for (var k = 0; k < rowCount; k++) {
							h = vv[k][i + 1];
							vv[k][i + 1] = s * vv[k][i] + c * h;
							vv[k][i] = c * vv[k][i] - s * h;
						}
					}
					p = -s * s2 * c3 * el1 * ee[l] / dl1;
					ee[l] = s * p;
					dd[l] = c * p;

					// Check for convergence.
				} while (Math.abs(ee[l]) > eps * tst1);
			}
			dd[l] = dd[l] + f;
			ee[l] = 0.0;
		}

		// Sort eigenvalues and corresponding vectors.
		for (var i = 0; i < rowCount - 1; i++) {
			var k = i;
			var p = dd[i];
			for (var j = i + 1; j < rowCount; j++) {
				if (dd[j] < p) {
					k = j;
					p = dd[j];
				}
			}
			if (k != i) {
				dd[k] = dd[i];
				dd[i] = p;
				for (var j = 0; j < rowCount; j++) {
					p = vv[j][i];
					vv[j][i] = vv[j][k];
					vv[j][k] = p;
				}
			}
		}

		vars.vv = vv;
		vars.ee = ee;
		vars.dd = dd;
		return vars;
	}
	var orthes = function(rowCount,vars) {
		var hh = vars.hh;
		var vv = vars.vv;
		var ort = vars.ort;

		var low = 0;
		var high = rowCount - 1;

		for (var m = low + 1; m <= high - 1; m++) {

			// Scale column.
			var scale = 0.0;
			for (var i = m; i <= high; i++)
				scale = scale + Math.abs(hh[i][m - 1]);
			if (scale != 0.0) {
				// Compute Householder transformation.
				var h = 0.0;
				for (var i = high; i >= m; i--) {
					ort[i] = hh[i][m - 1] / scale;
					h += ort[i] * ort[i];
				}

				var g = Math.sqrt(h);
				if (ort[m] > 0)
					g = -g;

				h = h - ort[m] * g;
				ort[m] = ort[m] - g;

				// Apply Householder similarity transformation
				// H = (I-u*u'/h)*H*(I-u*u')/h)

				for (var j = m; j < rowCount; j++) {
					var f = 0.0;
					for (var i = high; i >= m; i--)
						f += ort[i] * hh[i][j];
					f = f / h;
					for (var i = m; i <= high; i++)
						hh[i][j] -= f * ort[i];
				}

				for (var i = 0; i <= high; i++) {
					var f = 0.0;
					for (var j = high; j >= m; j--)
						f += ort[j] * hh[i][j];
					f = f / h;
					for (var j = m; j <= high; j++)
						hh[i][j] -= f * ort[j];
				}
				ort[m] = scale * ort[m];
				hh[m][m - 1] = scale * g;
			}
		}

		// Accumulate transformations (Algol's ortran).
		for (var i = 0; i < rowCount; i++)
			for (var j = 0; j < rowCount; j++)
				vv[i][j] = (i == j ? 1.0 : 0.0);

		for (var m = high - 1; m >= low + 1; m--) {
			if (hh[m][m - 1] != 0.0) {
				for (var i = m + 1; i <= high; i++)
					ort[i] = hh[i][m - 1];

				for (var j = m; j <= high; j++) {
					var g = 0.0;
					for (var i = m; i <= high; i++)
						g += ort[i] * vv[i][j];

					// Double division avoids possible underflow
					g = (g / ort[m]) / hh[m][m - 1];
					for (var i = m; i <= high; i++)
						vv[i][j] += g * ort[i];
				}
			}
		}

		vars.hh = hh;
		vars.vv = vv;
		vars.ort = ort;
		return vars;
	}
	var hqr2 = function(cdiv1, rowCount,vars) {
		var dd = vars.dd;
		var ee = vars.ee;
		var vv = vars.vv;
		var hh = vars.hh;

		// Initialize
		var nn = rowCount;
		var n = nn - 1;
		var low = 0;
		var high = nn - 1;
		var eps = Math.pow(2.0, -52.0);
		var exshift = 0.0;
		var p = 0, q = 0, r = 0, s = 0, z = 0, t, w, x, y;

		// Store roots isolated by balanc and compute matrix norm
		var norm = 0.0;
		for (var i = 0; i < nn; i++) {
			if (i < low | i > high) {
				dd[i] = hh[i][i];
				ee[i] = 0.0;
			}
			for (var j = Math.max(i - 1, 0); j < nn; j++)
				norm = norm + Math.abs(hh[i][j]);
		}

		// Outer loop over eigenvalue index
		var iter = 0;
		while (n >= low) {
			// Look for single small sub-diagonal element
			var l = n;
			while (l > low) {
				s = Math.abs(hh[l - 1][l - 1]) + Math.abs(hh[l][l]);
				if (s == 0.0)
					s = norm;
				if (Math.abs(hh[l][l - 1]) < eps * s)
					break;
				l--;
			}

			// Check for convergence
			// One root found
			if (l == n) {
				hh[n][n] = hh[n][n] + exshift;
				dd[n] = hh[n][n];
				ee[n] = 0.0;
				n--;
				iter = 0;
				// Two roots found
			}
			else if (l == n - 1) {
				w = hh[n][n - 1] * hh[n - 1][n];
				p = (hh[n - 1][n - 1] - hh[n][n]) / 2.0;
				q = p * p + w;
				z = Math.sqrt(Math.abs(q));
				hh[n][n] = hh[n][n] + exshift;
				hh[n - 1][n - 1] = hh[n - 1][n - 1] + exshift;
				x = hh[n][n];

				// Real pair
				if (q >= 0)  {
					if (p >= 0)
						z = p + z;
					else
						z = p - z;
					dd[n - 1] = x + z;
					dd[n] = dd[n - 1];
					if (z != 0.0)
						dd[n] = x - w / z;
					ee[n - 1] = 0.0;
					ee[n] = 0.0;
					x = hh[n][n - 1];
					s = Math.abs(x) + Math.abs(z);
					p = x / s;
					q = z / s;
					r = Math.sqrt(p * p + q * q);
					p = p / r;
					q = q / r;

					// Row modification
					for (var j = n - 1; j < nn; j++) {
						z = hh[n - 1][j];
						hh[n - 1][j] = q * z + p * hh[n][j];
						hh[n][j] = q * hh[n][j] - p * z;
					}

					// Column modification
					for (var i = 0; i <= n; i++) {
						z = hh[i][n - 1];
						hh[i][n - 1] = q * z + p * hh[i][n];
						hh[i][n] = q * hh[i][n] - p * z;
					}

					// Accumulate transformations
					for (var i = low; i <= high; i++) {
						z = vv[i][n - 1];
						vv[i][n - 1] = q * z + p * vv[i][n];
						vv[i][n] = q * vv[i][n] - p * z;
					}
					// Complex pair
				}
				else {
					dd[n - 1] = x + p;
					dd[n] = x + p;
					ee[n - 1] = z;
					ee[n] = -z;
				}
				n = n - 2;
				iter = 0;
				// No convergence yet
			}
			else {
				// Form shift
				x = hh[n][n];
				y = 0.0;
				w = 0.0;
				if (l < n) {
					y = hh[n - 1][n - 1];
					w = hh[n][n - 1] * hh[n - 1][n];
				}
				// Wilkinson's original ad hoc shift
				if (iter == 10) {
					exshift += x;
					for (var i = low; i <= n; i++)
						hh[i][i] -= x;
					s = Math.abs(hh[n][n - 1]) + Math.abs(hh[n - 1][n - 2]);
					x = y = 0.75 * s;
					w = -0.4375 * s * s;
				}
				// MATLAB's new ad hoc shift
				if (iter == 30) {
					s = (y - x) / 2.0;
					s = s * s + w;
					if (s > 0) {
						s = Math.sqrt(s);
						if (y < x)
							s = -s;
						s = x - w / ((y - x) / 2.0 + s);
						for (var i = low; i <= n; i++)
							hh[i][i] -= s;
						exshift += s;
						x = y = w = 0.964;
					}
				}
				iter = iter + 1;   // (Could check iteration count here.)
				// Look for two consecutive small sub-diagonal elements
				var m = n - 2;
				while (m >= l) {
					z = hh[m][m];
					r = x - z;
					s = y - z;
					p = (r * s - w) / hh[m + 1][m] + hh[m][m + 1];
					q = hh[m + 1][m + 1] - z - r - s;
					r = hh[m + 2][m + 1];
					s = Math.abs(p) + Math.abs(q) + Math.abs(r);
					p = p / s;
					q = q / s;
					r = r / s;
					if (m == l)
						break;
					if (Math.abs(hh[m][m - 1]) * (Math.abs(q) + Math.abs(r)) <
									eps * (Math.abs(p) * (Math.abs(hh[m - 1][m - 1]) + Math.abs(z) +
									Math.abs(hh[m + 1][m + 1]))))
					{
						break;
					}
					m--;
				}

				for (var i = m + 2; i <= n; i++) {
					hh[i][i - 2] = 0.0;
					if (i > m + 2)
						hh[i][i - 3] = 0.0;
				}

				// Double QR step involving rows l:n and columns m:n
				for (var k = m; k <= n - 1; k++) {
					var notlast = (k != n - 1);
					if (k != m) {
						p = hh[k][k - 1];
						q = hh[k + 1][k - 1];
						r = (notlast ? hh[k + 2][k - 1] : 0.0);
						x = Math.abs(p) + Math.abs(q) + Math.abs(r);
						if (x != 0.0) {
							p = p / x;
							q = q / x;
							r = r / x;
						}
					}
					if (x == 0.0)
						break;

					s = Math.sqrt(p * p + q * q + r * r);
					if (p < 0)
						s = -s;

					if (s != 0) {
						if (k != m)
							hh[k][k - 1] = -s * x;
						else if (l != m)
							hh[k][k - 1] = -hh[k][k - 1];
						p = p + s;
						x = p / s;
						y = q / s;
						z = r / s;
						q = q / p;
						r = r / p;

						// Row modification
						for (var j = k; j < nn; j++) {
							p = hh[k][j] + q * hh[k + 1][j];
							if (notlast) {
								p = p + r * hh[k + 2][j];
								hh[k + 2][j] = hh[k + 2][j] - p * z;
							}
							hh[k][j] = hh[k][j] - p * x;
							hh[k + 1][j] = hh[k + 1][j] - p * y;
						}

						// Column modification
						for (var i = 0; i <= Math.min(n, k + 3); i++) {
							p = x * hh[i][k] + y * hh[i][k + 1];
							if (notlast) {
								p = p + z * hh[i][k + 2];
								hh[i][k + 2] = hh[i][k + 2] - p * r;
							}
							hh[i][k] = hh[i][k] - p;
							hh[i][k + 1] = hh[i][k + 1] - p * q;
						}

						// Accumulate transformations
						for (var i = low; i <= high; i++) {
							p = x * vv[i][k] + y * vv[i][k + 1];
							if (notlast) {
								p = p + z * vv[i][k + 2];
								vv[i][k + 2] = vv[i][k + 2] - p * r;
							}
							vv[i][k] = vv[i][k] - p;
							vv[i][k + 1] = vv[i][k + 1] - p * q;
						}
					}  // (s != 0)
				}  // k loop
			}  // check convergence
		}  // while (n >= low)

		// Backsubstitute to find vectors of upper triangular form
		if (norm == 0.0)
			return;

		for (n = nn - 1; n >= 0; n--) {
			p = dd[n];
			q = ee[n];

			// Real vector
			if (q == 0) {
				var l = n;
				hh[n][n] = 1.0;
				for (var i = n - 1; i >= 0; i--) {
					w = hh[i][i] - p;
					r = 0.0;
					for (var j = l; j <= n; j++)
						r = r + hh[i][j] * hh[j][n];

					if (ee[i] < 0.0) {
						z = w;
						s = r;
					}
					else {
						l = i;
						if (ee[i] == 0.0) {
							if (w != 0.0)
								hh[i][n] = -r / w;
							else
								hh[i][n] = -r / (eps * norm);
							// Solve real equations
						}
						else {
							x = hh[i][i + 1];
							y = hh[i + 1][i];
							q = (dd[i] - p) * (dd[i] - p) + ee[i] * ee[i];
							t = (x * s - z * r) / q;
							hh[i][n] = t;
							if (Math.abs(x) > Math.abs(z))
								hh[i + 1][n] = (-r - w * t) / x;
							else
								hh[i + 1][n] = (-s - y * t) / z;
						}

						// Overflow control
						t = Math.abs(hh[i][n]);
						if ((eps * t) * t > 1)
							for (var j = i; j <= n; j++)
								hh[j][n] = hh[j][n] / t;
					}
				}
				// Complex vector
			}
			else if (q < 0) {
				var l = n - 1;

				// Last vector component imaginary so matrix is triangular
				if (Math.abs(hh[n][n - 1]) > Math.abs(hh[n - 1][n])) {
					hh[n - 1][n - 1] = q / hh[n][n - 1];
					hh[n - 1][n] = -(hh[n][n] - p) / hh[n][n - 1];
				}
				else {
					cdiv1 = cdiv(cdiv1, 0.0, -hh[n - 1][n], hh[n - 1][n - 1] - p, q);
					hh[n - 1][n - 1] = cdiv1.r;
					hh[n - 1][n] = cdiv1.i;
				}
				hh[n][n - 1] = 0.0;
				hh[n][n] = 1.0;

				for (var i = n - 2; i >= 0; i--) {
					var ra, sa, vr, vi;
					ra = 0.0;
					sa = 0.0;
					for (var j = l; j <= n; j++) {
						ra = ra + hh[i][j] * hh[j][n - 1];
						sa = sa + hh[i][j] * hh[j][n];
					}
					w = hh[i][i] - p;
					if (ee[i] < 0.0) {
						z = w;
						r = ra;
						s = sa;
					}
					else {
						l = i;
						if (ee[i] == 0) {
							cdiv1 = cdiv(cdiv1, -ra, -sa, w, q);
							hh[i][n - 1] = cdiv1.r;
							hh[i][n] = cdiv1.i;
						}
						else {
							// Solve complex equations
							x = hh[i][i + 1];
							y = hh[i + 1][i];
							vr = (dd[i] - p) * (dd[i] - p) + ee[i] * ee[i] - q * q;
							vi = (dd[i] - p) * 2.0 * q;
							if (vr == 0.0 & vi == 0.0)
								vr = eps * norm * (Math.abs(w) + Math.abs(q) + Math.abs(x) + Math.abs(y) + Math.abs(z));
							cdiv1 = cdiv(cdiv1, x * r - z * ra + q * sa, x * s - z * sa - q * ra, vr, vi);
							hh[i][n - 1] = cdiv1.r;
							hh[i][n] = cdiv1.i;
							if (Math.abs(x) > (Math.abs(z) + Math.abs(q))) {
								hh[i + 1][n - 1] = (-ra - w * hh[i][n - 1] + q * hh[i][n]) / x;
								hh[i + 1][n] = (-sa - w * hh[i][n] - q * hh[i][n - 1]) / x;
							}
							else {
								cdiv1 = cdiv(cdiv1, -r - y * hh[i][n - 1], -s - y * hh[i][n], z, q);
								hh[i + 1][n - 1] = cdiv1.r;
								hh[i + 1][n] = cdiv1.i;
							}
						}

						// Overflow control
						t = Math.max(Math.abs(hh[i][n - 1]), Math.abs(hh[i][n]));
						if ((eps * t) * t > 1) {
							for (var j = i; j <= n; j++) {
								hh[j][n - 1] = hh[j][n - 1] / t;
								hh[j][n] = hh[j][n] / t;
							}
						}
					}
				}
			}
		}

		// Vectors of isolated roots
		for (var i = 0; i < nn; i++) {
			if (i < low | i > high) {
				for (var j = i; j < nn; j++) {
					vv[i][j] = hh[i][j];
				}
			}
		}

		// Back transformation to get eigenvectors of original matrix
		for (var j = nn - 1; j >= low; j--) {
			for (var i = low; i <= high; i++) {
				z = 0.0;
				for (var k = low; k <= Math.min(j, high); k++)
					z = z + vv[i][k] * hh[k][j];
				vv[i][j] = z;
			}
		}

		vars.dd = dd;
		vars.ee = ee;
		vars.vv = vv;
		vars.hh = hh;
		return vars;
	}

	// vv -> iki boyutlu  -> V
	// hh -> iki boyutlu
	// dd -> tek boyutlu  -> RealEigenvalues
	// ee -> tek boyutlu  -> ImagEigenvalues
	// ort -> tek boyutlu
	var rowCount = mat.RowCount;
	var matGen = new MatrixGenerator(degree);
	var cdiv1 = [];
	cdiv1.r = 0;
	cdiv1.i = 0;
	var vars = [];
	vars.dd = matGen.Constant(1, rowCount, 0).Data;
	vars.ee = matGen.Constant(1, rowCount, 0).Data;
	vars.vv = matGen.Constant(rowCount, rowCount, 0).Data;
	var issymmetric = mat.IsSymmetric();
	if(issymmetric) {
		vars.vv = mat.Clone().Data;
		vars = tred2(rowCount, vars);  // Tridiagonalize.
		vars = tql2(rowCount, vars);   // Diagonalize.
	}
	else {
		vars.ort = matGen.Constant(1, rowCount, 0).Data;
		vars.hh = mat.Clone().Data;
		vars = orthes(rowCount, vars);  // Reduce to Hessenberg form.
		vars = hqr2(cdiv1, rowCount, vars);    // Diagonalize.
	}
	return new Eig(rowCount, vars.vv, vars.dd, vars.ee, degree);
}


var SVD = function(rowCount, colCount, u, v, s, degree) {
	var matGen = new MatrixGenerator(degree);
	var ss = matGen.Constant(rowCount, colCount, 0).Data;
	for (var i = 0; i < Math.min(colCount, rowCount); i++)
		ss[i][i] = s[i];

	this.U = new Matrix(u);
	this.V = new Matrix(v);
	this.S = new Matrix(ss);
	this.SingularValues = new Matrix(s);
}
var SingularValue_Decomposition = function(mat, degree) {
	var hypot = function(a, b) {
		var r;
		if (Math.abs(a) > Math.abs(b)){
			r = b / a;
			r = Math.abs(a) * Math.sqrt(1 + r*r);
		}
		else if (b != 0) {
			r = a / b;
			r = Math.abs(b) * Math.sqrt(1 + r*r);
		}
		else {
			r = 0;
		}
		return r;
	}

	var matGen = new MatrixGenerator(degree);
	var u, v;  // 2D array
	var s;     // 1D array
	var colCount = mat.ColCount;
	var rowCount = mat.RowCount;
	var a;     // 2D array
	var usingTranspose = false;
	if (rowCount < colCount) {
		// Use transpose and convert back at the end
		// Otherwise m < n case may yield incorrect results (see above comment)
		a = mat.Transpose().Data;
		usingTranspose = true;
		var temp = rowCount;
		rowCount = colCount;
		colCount = temp;
	}
	else
		a = mat.Clone().Data;

	var nu = Math.min(rowCount, colCount);
	s = matGen.Constant(1, Math.min(rowCount + 1, colCount), 0).Data[0];
	u = matGen.Constant(rowCount, rowCount, 0).Data;
	v = matGen.Constant(colCount, colCount, 0).Data;

	var e = matGen.Constant(1, colCount, 0).Data[0];
	var work = matGen.Constant(1, rowCount, 0).Data[0];
	var wantu = true;
	var wantv = true;

	// Reduce A to bidiagonal form, storing the diagonal elements
	// in s and the super-diagonal elements in e.
	var nct = Math.min(rowCount - 1, colCount);
	var nrt = Math.max(0, Math.min(colCount - 2, rowCount));

	for (var k = 0; k < Math.max(nct, nrt); k++) {
		if (k < nct) {
			// Compute the transformation for the k-th column and
			// place the k-th diagonal in s[k].
			// Compute 2-norm of k-th column without under/overflow.
			s[k] = 0;
			for (var i = k; i < rowCount; i++)
				s[k] = hypot(s[k], a[i][k]);

			if (s[k] != 0.0) {
				if (a[k][k] < 0.0)
					s[k] = -s[k];

				for (var i = k; i < rowCount; i++)
					a[i][k] /= s[k];

				a[k][k] += 1.0;
			}
			s[k] = -s[k];
		}

		for (var j = k + 1; j < colCount; j++) {
			if ((k < nct) & (s[k] != 0.0)) {
				// Apply the transformation.
				var t = 0;
				for (var i = k; i < rowCount; i++)
					t += a[i][k] * a[i][j];
				t = -t / a[k][k];
				for (var i = k; i < rowCount; i++)
					a[i][j] += t * a[i][k];
			}
			// Place the k-th row of A into e for the
			// subsequent calculation of the row transformation.
			e[j] = a[k][j];
		}

		if (wantu & (k < nct))
			// Place the transformation in U for subsequent back
			// multiplication.
			for (var i = k; i < rowCount; i++)
				u[i][k] = a[i][k];

		if (k < nrt) {
			// Compute the k-th row transformation and place the
			// k-th super-diagonal in e[k].
			// Compute 2-norm without under/overflow.
			e[k] = 0;
			for (var i = k + 1; i < colCount; i++)
				e[k] = hypot(e[k], e[i]);

			if (e[k] != 0.0) {
				if (e[k + 1] < 0.0)
					e[k] = -e[k];

				for (var i = k + 1; i < colCount; i++)
					e[i] /= e[k];
				e[k + 1] += 1.0;
			}
			e[k] = -e[k];

			if ((k + 1 < rowCount) & (e[k] != 0.0)) {
				// Apply the transformation.
				for (var i = k + 1; i < rowCount; i++)
					work[i] = 0.0;

				for (var j = k + 1; j < colCount; j++)
					for (var i = k + 1; i < rowCount; i++)
						work[i] += e[j] * a[i][j];

				for (var j = k + 1; j < colCount; j++) {
					var t = -e[j] / e[k + 1];
					for (var i = k + 1; i < rowCount; i++)
						a[i][j] += t * work[i];
				}
			}

			if (wantv)
				// Place the transformation in V for subsequent
				// back multiplication.
				for (var i = k + 1; i < colCount; i++)
					v[i][k] = e[i];
		}
	}

	// Set up the final bidiagonal matrix or order p.
	var p = Math.min(colCount, rowCount + 1);
	if (nct < colCount)
		s[nct] = a[nct][nct];

	if (rowCount < p)
		s[p - 1] = 0.0;

	if (nrt + 1 < p)
		e[nrt] = a[nrt][p - 1];
	e[p - 1] = 0.0;

	// If required, generate U.
	if (wantu) {
		for (var j = nct; j < nu; j++) {
			for (var i = 0; i < rowCount; i++)
				u[i][j] = 0.0;
			u[j][j] = 1.0;
		}

		for (var k = nct - 1; k >= 0; k--) {
			if (s[k] != 0.0) {
				for (var j = k + 1; j < nu; j++) {
					var t = 0;
					for (var i = k; i < rowCount; i++)
						t += u[i][k] * u[i][j];
					t = -t / u[k][k];

					for (var i = k; i < rowCount; i++)
						u[i][j] += t * u[i][k];
				}

				for (var i = k; i < rowCount; i++)
					u[i][k] = -u[i][k];
				u[k][k] = 1.0 + u[k][k];

				for (var i = 0; i < k - 1; i++)
					u[i][k] = 0.0;
			}
			else {
				for (var i = 0; i < rowCount; i++)
					u[i][k] = 0.0;
				u[k][k] = 1.0;
			}
		}
	}

	// If required, generate V.
	if (wantv) {
		for (var k = colCount - 1; k >= 0; k--) {
			if ((k < nrt) & (e[k] != 0.0)) {
				for (var j = k + 1; j < nu; j++) {
					var t = 0;
					for (var i = k + 1; i < colCount; i++)
						t += v[i][k] * v[i][j];
					t = -t / v[k + 1][k];

					for (var i = k + 1; i < colCount; i++)
						v[i][j] += t * v[i][k];
				}
			}

			for (var i = 0; i < colCount; i++)
				v[i][k] = 0.0;
			v[k][k] = 1.0;
		}
	}

	// Main iteration loop for the singular values.
	var pp = p - 1;
	var iter = 0;
	var eps = Math.pow(2.0, -52.0);
	var tiny = Math.pow(2.0, -966.0);

	while (p > 0) {
		var k, kase;

		// Here is where a test for too many iterations would go.

		// This section of the program inspects for
		// negligible elements in the s and e arrays.  On
		// completion the variables kase and k are set as follows.

		// kase = 1     if s(p) and e[k-1] are negligible and k<p
		// kase = 2     if s(k) is negligible and k<p
		// kase = 3     if e[k-1] is negligible, k<p, and
		//              s(k), ..., s(p) are not negligible (qr step).
		// kase = 4     if e(p-1) is negligible (convergence).

		for (k = p - 2; k >= -1; k--) {
			if (k == -1)
				break;

			if (Math.abs(e[k]) <= tiny + eps * (Math.abs(s[k]) + Math.abs(s[k + 1]))) {
				e[k] = 0.0;
				break;
			}
		}

		if (k == p - 2) {
			kase = 4;
		}
		else {
			var ks;
			for (ks = p - 1; ks >= k; ks--) {
				if (ks == k)
					break;
				var t = (ks != p ? Math.abs(e[ks]) : 0) + (ks != k + 1 ? Math.abs(e[ks - 1]) : 0); // --0.
				if (Math.abs(s[ks]) <= tiny + eps * t) {
					s[ks] = 0.0;
					break;
				}
			}

			if (ks == k) {
				kase = 3;
			}
			else if (ks == p - 1) {
				kase = 1;
			}
			else {
				kase = 2;
				k = ks;
			}
		}
		k++;

		// Perform the task indicated by kase.
		var f = 0;
		switch (kase) {

			// Deflate negligible s(p).
			case 1:
				f = e[p - 2];
				e[p - 2] = 0.0;
				for (var j = p - 2; j >= k; j--) {
					var t = hypot(s[j], f);
					var cs = s[j] / t;
					var sn = f / t;
					s[j] = t;
					if (j != k) {
						f = -sn * e[j - 1];
						e[j - 1] = cs * e[j - 1];
					}

					if (wantv) {
						for (var i = 0; i < colCount; i++) {
							t = cs * v[i][j] + sn * v[i][p - 1];
							v[i][p - 1] = -sn * v[i][j] + cs * v[i][p - 1];
							v[i][j] = t;
						}
					}
				}
				break;

			// Split at negligible s(k).
			case 2:
				f = e[k - 1];
				e[k - 1] = 0.0;
				for (var j = k; j < p; j++) {
					var t = hypot(s[j], f);
					var cs = s[j] / t;
					var sn = f / t;
					s[j] = t;
					f = -sn * e[j];
					e[j] = cs * e[j];
					if (wantu) {
						for (var i = 0; i < rowCount; i++) {
							t = cs * u[i][j] + sn * u[i][k - 1];
							u[i][k - 1] = -sn * u[i][j] + cs * u[i][k - 1];
							u[i][j] = t;
						}
					}
				}
				break;

			// Perform one qr step
			case 3:
				// Calculate the shift.
				var scale = Math.max(Math.max(Math.max(Math.max(
                          Math.abs(s[p - 1]), Math.abs(s[p - 2])), Math.abs(e[p - 2])), Math.abs(s[k])), Math.abs(e[k]));

				var sp = s[p - 1] / scale;
				var spm1 = s[p - 2] / scale;
				var epm1 = e[p - 2] / scale;
				var sk = s[k] / scale;
				var ek = e[k] / scale;
				var b = ((spm1 + sp) * (spm1 - sp) + epm1 * epm1) / 2.0;
				var c = (sp * epm1) * (sp * epm1);
				var shift = 0.0;
				if ((b != 0.0) | (c != 0.0)) {
					shift = Math.sqrt(b * b + c);
					if (b < 0.0)
						shift = -shift;
					shift = c / (b + shift);
				}
				f = (sk + sp) * (sk - sp) + shift;
				var g = sk * ek;

				// Chase zeros.
				for (var j = k; j < p - 1; j++) {
					var t = hypot(f, g);
					var cs = f / t;
					var sn = g / t;
					if (j != k)
						e[j - 1] = t;

					f = cs * s[j] + sn * e[j];
					e[j] = cs * e[j] - sn * s[j];
					g = sn * s[j + 1];
					s[j + 1] = cs * s[j + 1];
					if (wantv) {
						for (var i = 0; i < colCount; i++) {
							t = cs * v[i][j] + sn * v[i][j + 1];
							v[i][j + 1] = -sn * v[i][j] + cs * v[i][j + 1];
							v[i][j] = t;
						}
					}
					t = hypot(f, g);
					cs = f / t;
					sn = g / t;
					s[j] = t;
					f = cs * e[j] + sn * s[j + 1];
					s[j + 1] = -sn * e[j] + cs * s[j + 1];
					g = sn * e[j + 1];
					e[j + 1] = cs * e[j + 1];
					if (wantu && (j < rowCount - 1)) {
						for (var i = 0; i < rowCount; i++) {
							t = cs * u[i][j] + sn * u[i][j + 1];
							u[i][j + 1] = -sn * u[i][j] + cs * u[i][j + 1];
							u[i][j] = t;
						}
					}
				}
				e[p - 2] = f;
				iter = iter + 1;
				break;

			// Convergence.
			case 4:
				// Make the singular values positive.
				if (s[k] <= 0.0) {
					s[k] = (s[k] < 0.0 ? -s[k] : 0.0);
					if (wantv) {
						for (var i = 0; i <= pp; i++) {
							v[i][k] = -v[i][k];
						}
					}
				}

				// Order the singular values.
				while (k < pp) {
					if (s[k] >= s[k + 1]) {
						break;
					}
					var t = s[k];
					s[k] = s[k + 1];
					s[k + 1] = t;
					if (wantv && (k < colCount - 1)) {
						for (var i = 0; i < colCount; i++) {
							t = v[i][k + 1];
							v[i][k + 1] = v[i][k];
							v[i][k] = t;
						}
					}

					if (wantu && (k < rowCount - 1)) {
						for (var i = 0; i < rowCount; i++) {
							t = u[i][k + 1];
							u[i][k + 1] = u[i][k];
							u[i][k] = t;
						}
					}
					k++;
				}
				iter = 0;
				p--;
				break;
		}
	}

	if (usingTranspose) {
		var temp = rowCount;
		rowCount = colCount;
		colCount = temp;
		var tempA = u;
		u = v;
		v = tempA;
	}

	return new SVD(rowCount, colCount, u, v, s, degree);
}
