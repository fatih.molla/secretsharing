var ErrorCorrectingCode = function(generator, q) {
	var processor = new MatrixCalculator(q);
	var matGen = new MatrixGenerator(q);
	var n = generator.ColCount;  // uzunluk
	var k = generator.RowCount;  // kodlanacak kelimenin uzunluğu
	var M = Math.pow(q, k);      // kelime sayısı

	var aggregate = function(arr, seed, func) {
		var accumulate = seed;
		for(var i = 0; i < arr.length; i++) {
			accumulate = func(accumulate, arr[i]);
		}
		return accumulate;
	}
	var findWords = function(q, k) {
		var wordCount = Math.pow(q, k);
		var data = [];
		for (var j = 0; j < k; j++) {
			var row = [];
			var d = -1;
			for (var i = 0; i < wordCount; i++) {
				if (i % Math.pow(q, j) == 0)
						d += 1;
				row.push(d % q);
			}
			data.push(row);
		}
		var mat =  new Matrix(data.reverse()).Transpose();
		var rows = [];
		for(var i = 0; i < mat.RowCount; i++)
			rows.push(new CodeWord(new Matrix(mat.GetRow(i))));
		return rows;
		//return new Matrix(data.reverse()).Transpose();
	}
	var calculateCodeword = function(codeword, generatorMatrix) {
		return new CodeWord(processor.Multiply(codeword.Data, generatorMatrix));
	}
	var hammingDistance = function(codeword1, codeword2) {
		var result = 0;
		for (var i = 0; i < n; i++) {
			if (codeword1.Get(i) != codeword2.Get(i))
				result++;
		}
		return result;
	}
	var isSubset = function(set1, set2) {
		var subset = true;
		for (var i = 0; i < set1.length; i++)
			if(set2.indexOf(set1[i]) < 0){
				subset = false;
				break;
			}
		return subset;
	}
	
	var factorial = function(num) {
		var result  = 1;
		for(var i = 1; i <= num; i++)
			result *= i;
		return result;
	}
	var combination = function(n,k) {
		return factorial(n) / (factorial(k) * factorial(n-k));
	}
	
	
	//var t = Math.trunc((d-1)/2);
	//var rightSide = Math.pow(q, n);
	//var leftSide = 0;
	//for (var i = 0; i <= t; i++)
	//	leftSide += combination(n, i) * Math.pow(q-1,i);
	//leftSide *= M;

	this.q = q; // Degree of field
	this.n = n; //
	this.k = k;
	this.M = M;
	//this.d = d;
	//this.t = t;
	//this.isPerfect = (leftSide == rightSide);
	//this.SourceWords = words;
	//this.CodeWords = codeWords;
	this.Generator = generator;
	// this.GeneratorSF = generatorSF;
	// this.PartiyCheckSF = parityCheckSF;
	this.CalculateCodeword = function(codeWord) { return calculateCodeword(codeWord, this.Generator); };
	//this.FindWords = findWords;
	this.DualCode = function() {
		var matrix = this.Generator.Clone();
		var calculator = new MatrixCalculator(this.q);
		var generator = new MatrixGenerator(this.q);
		var vectors = [];
		
		for (var b = 0; b < this.n-this.k; b++) {
			var rows = [0];
			var m = matrix.GetCol(0);
			if (m.Get(0,0) == 0){
				m = m.RemoveLastCol();
				rows = [];
			}
			for (var i = 1; i < matrix.ColCount; i++){
				m = m.AddRight(matrix.GetCol(i));
				var g = calculator.Eliminate2(m);
				var diag = g.GetDiag();
				if (diag.HasZero())
					m = m.RemoveLastCol();
				else 
					rows.push(i);
				
				if (matrix.RowCount == m.ColCount)
					break;
			}
			
			if (m.RowCount != m.ColCount) {
				return null;
			}
			
			var indexes = [];
			for (var i = 0; i < matrix.ColCount; i++) {
				if(rows.indexOf(i) == -1)
					indexes.push(i);
			}
			
			var randValues = [];
			for (var i = 0; i < indexes.length; i++) {
				if (i != b)
					randValues.push(0);
				else randValues.push(1);
			}
			randValues = new Matrix(randValues);

			
			var results = [];
			for (var i = 0; i < matrix.RowCount; i++) {
				var res = 0;
				for (var j = 0; j < indexes.length; j++) {
					res += matrix.Get(i, indexes[j]) * randValues.Get(0, j);
				}
				res = calculator.EvaluateDegree(-1 * calculator.EvaluateDegree(res));
				results.push(res);
			}
			var resultMatrix = new Matrix(results).Transpose();
			var resultValues = calculator.Solve(m, resultMatrix);
			var vector = generator.Constant(1, matrix.ColCount, 0).Data[0];
			for (var i = 0; i < matrix.ColCount; i++) {
				if (rows.indexOf(i) >= 0)
					vector[i] = resultValues.Get(0, rows.indexOf(i));
				if (indexes.indexOf(i) >= 0)
					vector[i] = randValues.Get(0, indexes.indexOf(i));
			}
			vectors.push(vector);
		}
		//AddFormula("divResult", "parity_check_matrix6", new Matrix(rows).ToLatex());
		//AddFormula("divResult", "parity_check_matrix5", m.ToLatex());
		//AddFormula("divResult", "parity_check_matrix8", randValues.ToLatex());
		//AddFormula("divResult", "parity_check_matrix9", resultMatrix.ToLatex());
		//AddFormula("divResult", "parity_check_matrix7", resultValues.ToLatex());

		return new ErrorCorrectingCode(new Matrix(vectors), this.q);
	}
	this.GetWord = function (i) {
		
		var data = [];
		do {
			
			var r = i % this.q;
			i = parseInt(i / this.q); 
			data.push(r);
			if (i  < this.q)
				data.push(i);
			
		} while(i >= this.q)
		
		var dim = this.k - data.length;
		for (var k = 0; k < dim; k++) {
			data.push(0);
		}
		data = data.reverse();
		return new CodeWord(new Matrix(data));
	}
	this.Syndroms = function () {
		
		var processor = new MatrixCalculator(this.q);
		var hT = this.DualCode().Generator.Transpose();
		var dim = this.n - this.k;
		var syndroms = get_word_all(this.q, dim);
		var words = get_word_all(this.q, this.n);
		var result = {};
		for (var i = 0; i < syndroms.length; i++) {
			result[syndroms[i].Text] = null;
		}
		for (var i = 0; i < words.length; i++) {
			var syd = new CodeWord(processor.Multiply(words[i].Data, hT));
			var w = result[syd.Text];
			if (w == null || w.weight > words[i].weight)
				result[syd.Text] = words[i];
			console.log(syd.Text + " " + words[i].Text);
		}
		console.log("");
		return result;
	}
	this.Decode = function (codeWord) {
		
		vectors = [];
		var gen = new MatrixGenerator(q);
		var processor = new MatrixCalculator(q);
		var dim = this.n - this.k;
		var c = combination(this.n, dim);
		
		
		//for (var i = 0; i < n; i++) {
		//	var add;
		//	do {
		//		add = true;
		//		var vector;
		//		do {
		//			var vector = gen.RandomInt(r, 1, 0, q+1);
		//			var sum = new MatrixCalculator().Sum(vector);
		//		} while (sum == 0)
		//		
		//		for (var j = 0; j < i; j++) {
		//			var v = vectors[j];
		//			var s = processor.IsScalerMultiplier(v, vector);
		//			if (s == true) {
		//				add = false;
		//				break;
		//			}
		//		}
		//	} while(!add)
		//	
		//	vectors.push(vector);
		//}
		//
		//var matrix = vectors[0];
		//for (var i = 1; i < vectors.length; i++)
		//	matrix = matrix.AddRight(vectors[i]);
		
	}
	
	
	var get_word_all = function(q, k) {
		var result = [];
		var syndromCount = Math.pow(q, k);
		for (var i = 0; i < syndromCount; i++) {
			var word = get_word(q, k, i);
			result.push(word);
		}
		return result;
	}
	var get_word = function(q, k, i) {
		
		var data = [];
		do {
			
			var r = i % q;
			i = parseInt(i / q); 
			data.push(r);
			if (i  < q)
				data.push(i);
			
		} while(i >= q)
		
		var dim = k - data.length;
		for (var j = 0; j < dim; j++) {
			data.push(0);
		}
		data = data.reverse();
		return new CodeWord(new Matrix(data));
	}
}

var CodeWord = function(mat) {
	var aggregate = function(mat, seed, func) {
		var accumulate = seed;
		for(var i = 0; i < mat.RowCount; i++) {
			for(var j = 0; j < mat.ColCount; j++)
				accumulate = func(accumulate, mat.Get(0,i * mat.RowCount + j));
		}
		return accumulate;
	}

	var funcText = function(accumulate, value) { return accumulate + value; };
	var funcSupport = function(accumulate, value) { return accumulate + ',' + value; };
	var funcWeight = function(accumulate, value) { return accumulate + (value == 0 ? 0 : 1); };


	var support = [];
	var supportText = "";
	for(var i = 0; i < mat.ColCount; i++) {
		if (mat.Get(0,i) != 0)
			support.push(i);
	}
	if(support.length > 0) {
		var text = aggregate(new Matrix(support), "", funcSupport);
		supportText = "\\{" + text.substring(1, text.length) + "\\}";
	}
	else supportText = "\\varnothing";

	this.Data = mat;
	this.Text = aggregate(this.Data, "", funcText);
	this.weight = aggregate(this.Data, 0, funcWeight)
	this.length = mat.ColCount;
	this.support = support;
	this.supportText = supportText;
	this.isMinimalCodeword = false;
	this.isZero = support.length == 0 ? true : false;
	this.Get = function(i) {
		return this.Data.Get(0,i);
	}
	this.GetWriteOrder = function(i) {
		return this.Data.Get(0,this.length-i-1);
	}
	this.Equals = function(codeword){
		if (codeword.length != this.length)
			return false;
		for (var i = 0; i < this.length; i++)
			if (this.Get(i) != codeword.Get(i))
				return false;
		return true;
	}

}
